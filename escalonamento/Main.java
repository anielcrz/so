package escalonamento;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

 


public class Main {

	private static Scanner scanner;

	public static void main(String[] args) throws FileNotFoundException {
		scanner = new Scanner(new File("cenario1.txt"));
		Scanner entrada = scanner; 
		ArrayList<Processo> lista = new ArrayList<Processo>();
		int id=1, tempo_exe,  tempo_sub,  tempo_bloq,  prior; 
		while(entrada.hasNext() ) {
			String linha = entrada.next();
			String[] parts = linha.split(",");
			int[] ints = new int[parts.length];
			for (int i = 0; i < parts.length; i++) {
			    ints[i] = Integer.parseInt(parts[i]);
			}
			id = ints[0];
			tempo_sub = ints[1] ;
			prior = ints[2] ;
			tempo_exe = ints[3] ;
			tempo_bloq = ints[4] ;
            lista.add(new Processo(id, tempo_exe, tempo_sub, tempo_bloq, prior));
			
            
            
        }
        entrada.close();

	}
	
}
